var http = require('http');

var data = {
    info: [{
        name: 'John Doe',
        age: 35,
        address: '123 Main St.',
        city: 'Anytown',
        state: 'CA',
        zip: '12345'
    },
    {
        name: 'Jane Doe',
        age: 32,
        address: '987 Elm St.',
        city: 'Anytown',
        state: 'CA',
        zip: '54321'
    }]
};

let PORT = 5000;

http.createServer(function (request, response) {
    response.writeHead(200, {'Content-Type': 'text/plain'});
    response.end(JSON.stringify(data.info));
}).listen(PORT);